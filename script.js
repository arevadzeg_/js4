// 1. Check Winner Team
function calcAverage(array) {
  sum = 0;
  for (let element of array) {
    sum += element;
  }
  return sum / array.length;
}

function checkWinner(teamOne, teamTwo) {
  teamOne = calcAverage(teamOne);
  teamTwo = calcAverage(teamTwo);

  if (teamOne > teamTwo * 2) {
    console.log(`Winner is PowerRangers with ${teamOne} points`);
  } else if (teamTwo > teamOne * 2) {
    console.log(`Winner is FairyTails ${teamTwo} points`);
  } else {
    console.log("No winner this time");
  }
}

teams = {
  PowerRangers: {
    scores: [
      [44, 23, 71], 
      [85, 54, 41],
    ],
  },
  FairyTails: {
    scores: [
      [65, 54, 49],
      [23, 34, 47],
    ],
  },
};

for (let i in teams.PowerRangers.scores) {
  checkWinner(teams.PowerRangers.scores[i], teams.FairyTails.scores[i]);
}

// 2. Tip Calculator

const ludovico = {
  ammount: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
  tip: [],
  total: [],
};

function calcTip(ammount) {
  return ammount > 50 && ammount < 300 ? ammount * 0.15 : ammount * 0.2;
}

for (let i of ludovico.ammount) {
  tip = calcTip(i);
  ludovico.tip.push(tip);
  ludovico.total.push(tip + i);
}

function calculateAverage(array) {
  sum = 0;
  for (let i of array) {
    sum += i;
  }
  return sum / array.length;
}

ludovico["totalAverage"] = calculateAverage(ludovico.total);
ludovico["tipAverage"] = calculateAverage(ludovico.tip);

console.log(ludovico);
